#include <stdio.h>
#include <limits.h>

int main(){
	printf("type\t\tmin\tmax\n");

	// chars
	printf("char\t\t%d\t%d\n", CHAR_MIN, CHAR_MAX);
	printf("signed char\t%d\t%d\n", SCHAR_MIN, SCHAR_MAX);
	printf("unsigned char\t%d\t%d\n", 0, UCHAR_MAX);

	// shorts
	printf("short\t\t%d\t%d\n", SHRT_MIN, SHRT_MAX);
	printf("unsigned short\t%d\t%d\n", 0, USHRT_MAX);

	// ints
	printf("int\t\t%d\t%d\n", INT_MIN, INT_MAX);
	printf("unsigned int\t%d\t%ld\n", 0, UINT_MAX);

	// longs
	printf("long\t\t%ld\t%ld\n", LONG_MIN, LONG_MAX);
	printf("unsigned long\t%lu\t%lu\n", 0, ULONG_MAX);

}
