#include <stdio.h>
#include <math.h>

#define MAXLENGTH 15	//maximum length of input string

char line[MAXLENGTH];

long hstoi(int i);
int hctoi(char c);
int getline2(void);

/*
	A program that tests the hstoi() function that I've implemented below.
	Should compile with: `gcc ex2_3.c -lm`
*/
int main(){
	extern char line[];
	int lc;
	long res;
	while (1){
		lc = getline2();
		if (lc == -1) {
			printf("ERROR: line is longer than MAXLENGTH, which's current value: %d\n", MAXLENGTH-1);
			continue;
		}
		if (line[0] == 'q'){
			printf("Exiting...\n");
			return 1;
		}
		if ((res = hstoi(lc)) == -1)
			printf("ERROR: %s is not hexadecimal!\n");
		else
			printf("%s in decimal: %ld\n", line, res);
	}

}

// hexa string to integer (long)
long hstoi(int i){
	long result = 0L;
	int pr, pr2;	// partial result
	extern char line[];

	//skip the end-of-string (and newline characters -> skipped in getline2)
	i-=1;
	for (int j = 0; i >=0 && line[i] != 'x' && line[i] != 'X'; --i) {
		if ((pr = hctoi(line[i])) == -1) {
			return -1;
		}
//		printf("j, i, c, 16^j: %i, %i, %c, %l\n", j, i, line[i], pow(16L, (long)j));
		result += pr * pow(16L, (long)j);
		++j;
	}
	return result;
}

//hexa char to integer (long)
int hctoi(char c){
	if ('0' <= c && c <= '9')
		return c - '0';
	else if ('a' <= c && c <= 'f')
		return 10 + c - 'a';
	else if ('A' <= c && c <= 'F')
		return 10 + c - 'A';
	else
		return -1;
}

int getline2(void){
	extern char line[];
	int i, c;

	// read until `MAXLENGTH` cause we don't store newline char
	for (i = 0; i < MAXLENGTH && (c = getchar()) != EOF && c!= '\n'; ++i)
		line[i] = c;
	// if last char is not newline, then the line is longer than this program can process
	if (c != '\n') {
		// read every char until end of...
		while ((c = getchar()) != EOF && c != '\n')
			;;
		return -1;
	}

	line[i] = '\0';

	return i;
}
