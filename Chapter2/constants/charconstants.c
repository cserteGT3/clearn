#include <stdio.h>

int main(){
	printf("\\a:\t%c\n", '\a');
	printf("\\v:\t%c\n", '\v');
	printf("\\\\:\t%c\n", '\\');
	printf("\\?:\t%c\n", '\?');
	printf("\\':\t%c\n", '\'');
	printf("\\0:\t%c\n", '\0');
	printf("\\0's value:\t%d\n", '\0');
}
