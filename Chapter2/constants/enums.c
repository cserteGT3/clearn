#include <stdio.h>

enum months {JAN = 1, FEB, MAR, APR, MAY, JUN,
				JUL, AUG, SEP, OCT, NOV, DEC};

int main(){
	int m1 = 2;
	if (m1 == FEB)
		printf("m1 is february\n");
	m1++;
	if (m1 == MAR)
		printf("m1 is march, thus should be 3: %d\n", m1);
}
