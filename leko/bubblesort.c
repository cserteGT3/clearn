#include <stdio.h>

#define N 10
#define FALSE 0
#define TRUE 1

int readdigit(void);
void printdigitarray(int da[]);
void bubblesort(int da[]);

int main()
{
    int numbers[10];
    // read 10 numbers/digits
    for (int i = 0; i < N; ++i)
        numbers[i] = readdigit();
    // print "input"
    printdigitarray(numbers);
    // sort array
    bubblesort(numbers);
    // print output
    printdigitarray(numbers);
}


int readdigit(void)
{
    int c;
    printf("Enter one digit:");
    scanf("%d", &c);
    if (c < 0 || c > 9)
        printf("That's not a digit...\n");
    return c;
}

void printdigitarray(int da[])
{
    for (int i = 0; i < N; ++i)
        printf("%d ", da[i]);
    printf("\n");
}

void bubblesort(int da[])
{
    int finished, tempv;
    do {
        finished = TRUE;
        for (int i = 0; i < N-1; ++i) {
            if (da[i] > da[i+1]) {
                finished = FALSE;
                tempv = da[i];
                da[i] = da[i+1];
                da[i+1] = tempv;
            }
        }
    } while (! finished);
}
