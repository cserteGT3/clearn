#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10
#define TRUE 1
#define FALSE 0

// print the table: 2d array
void printdigitarray(char da[][N]);
// init a table to 0s
void initchararray(char da[][N]);
// get one coordinate; separated by space
int readcoordinate(int positions[]);
// get the 10 player coordinates
void getplayercoords(char da[][N]);
// randomcoordinates - load a random, valid coordinate to readto array
void randomcoordinates(void);
// load computer coordinates
void loadcomputercoords(char da[][N]);
// player's round
int playersturn(char da[][N]);
// computer's
int computersturn(char da[][N]);
// gameloop, that returns with the number of rounds
// number is negative if player wins, and positive if computer
int gameloop(char tableA[][N], char tableB[][N]);


// an array to store the read coordinates
int readto[2];



int main()
{
    /* allocations */
    // player
    char playerA[N][N];
    // computer
    char playerB[N][N];
    // init to 0s
    initchararray(playerA);
    initchararray(playerB);

    /* setup */
    getplayercoords(playerA);
    printf("Your table:\n");
    printdigitarray(playerA);
    srand(time(NULL));
    loadcomputercoords(playerB);
    printf("Your opponent's table:\n");
    printdigitarray(playerB);

    /* game loop */
    int result = gameloop(playerA, playerB);
    if (result < 0) {
        result = -1*result;
        printf("You won ");
    } else {
        printf("Computer won ");
    }
    printf("in %d rounds!\n", result);
    printf("Restart the program for a new game!\n");
}

void printdigitarray(char da[][N])
{
    for (char i = 0; i < N; ++i) {
        for (char j = 0; j < N; ++j)
            printf("%d ", da[i][j]);
        printf("\n");
    }
}

void initchararray(char da[][N])
{
    for (char i = 0; i < N; ++i)
        for (char j = 0; j < N; ++j)
            da[i][j] = 0;
}

int readcoordinate(int positions[])
{
    int posx, posy;
    printf("Give a coordinate:");
    scanf("%d %d", &posx, &posy);
    if (0 <= posx && posx < N && 0 <= posy && posy < N) {
        positions[0] = posx;
        positions[1] = posy;
        return TRUE;
    }
    return FALSE;
}

void getplayercoords(char da[][N])
{
    int valid = 0;
    extern int readto[2];
    printf("Give 10 coordinates: a pair of integers from 0 to 9, separated by space!\n");
    printf("Don't give the same coordinates twice please...\n");
    while (valid < N) {
        if (readcoordinate(readto) == TRUE) {
            da[readto[0]][readto[1]] = 1;
            valid++;
        }
    }
}

void randomcoordinates(void)
{
    extern int readto[2];
    readto[0] = rand()%10;
    readto[1] = rand()%10;
}

void loadcomputercoords(char da[][N])
{
    extern int readto[2];
    int valids = 0;
    int val1, val2;
    while (valids < N) {
        randomcoordinates();
        val1 = readto[0];
        val2 = readto[1];
        if (! da[val1][val2]) {
            da[val1][val2] = 1;
            valids++;
        }
    }
}

int playersturn(char da[][N])
{
    extern int readto[2];
    int res = FALSE;
    while (!res) {
        res = readcoordinate(readto);
        if (!res)
            printf("Please give coordinate from 0 to 9!\n");

    }
    if (da[readto[0]][readto[1]]) {
        // 1 -> score changes
        // set the value at coordinate to 0
        da[readto[0]][readto[1]] = 0;
        return 1;
    } else {
        // 0 -> score doesn't change
        return 0;
    }
}

int computersturn(char da[][N])
{
    extern int readto[2];
    randomcoordinates();
    printf("Computer guessed: %d %d\n", readto[0], readto[1]);
    if (da[readto[0]][readto[1]]) {
        // 1 -> score changes
        // set the value at coordinate to 0
        da[readto[0]][readto[1]] = 0;
        return 1;
    } else {
        // 0 -> score doesn't change
        return 0;
    }
}



// gameloop, that returns with the number of rounds
// number is negative if player wins, and positive if computer
int gameloop(char playerA[][N], char playerB[][N])
{
    // store score
    int scoreA = 0;
    int scoreB = 0;
    // number of rounds
    int roundcount = 0;
    // indicate if player scored
    int turnI = 0;
    while (TRUE) {
        ++roundcount;
        /* player's turn */
        printf("######################\n");
        printf("#####  Round %d!  ####\n", roundcount);
        printf("######################\n");
        printf("It's your turn!\n");
        // playersturn gets the opponent's table
        turnI = playersturn(playerB);
        if (turnI)
            printf("You've hit. ");
        else
            printf("You've missed. ");
        scoreA += turnI;
        printf("Your score is: %d\n", scoreA);
        /* return if score is 10 */
        if (scoreA == 10)
            return -1*roundcount;

        /* computers turn*/
        turnI = computersturn(playerA);
        if (turnI)
            printf("Computer've hit. ");
        else
            printf("Computer've missed. ");
        scoreB += turnI;
        printf("Computer's score is: %d\n", scoreB);
        printf("Your table:\n");
        printdigitarray(playerA);
        /* return if score is 10 */
        if (scoreB == 10)
            return roundcount;
    }
}
