#include <stdio.h>

#define N 20

void printdigitarray(int da[]);
void fibonacci(int da[], int length);

int main()
{
    // allocate
    printf("Add meg, hogy meddig menjen a fibonacci sorozat: ");
    int fibo;
    scanf("%d",&fibo);
    int numbers[fibo];
    // compute
    fibonacci(numbers, fibo);
    // print
    printdigitarray(numbers);
}

void printdigitarray(int da[])
{
    for (int i = 0; i < N; ++i)
        printf("%d ", da[i]);
    printf("\n");
}

void fibonacci(int da[], int length)
{
    da[0] = 0;
    da[1] = 1;
    for (int i = 2; i < length; ++i)
        da[i] = da[i-1] + da[i-2];
}
