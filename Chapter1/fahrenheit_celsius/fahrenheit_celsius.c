/* print Fahrenheit-Celsius table 
	for f = 0, 20 ... 300 */
main()
{
	int lower, upper, step;
	float fahr, celsius;

	lower = 0; // lower limit of temperature table
	upper = 300; /* upper limit */
	step = 20; // step size

	printf("Fahrenheit-Celsius conversion table\n"); // printing a header
	fahr = lower;
	while (fahr <= upper){
		celsius = (5.0/9.0) * (fahr-32.0);
		printf("%4.0f %6.1f\n", fahr, celsius);
		fahr = fahr + step;
	}
	printf("\n\n");
	printf("Celsius-Fahrenheit conversion table\n");
	for (celsius = 0; celsius <= upper; celsius = celsius+step)
		printf("%4.0f %6.1f\n", celsius, celsius/(5.0/9.0)+32.0);

	int fahr2;
	printf("\n\n");
	printf("Fahrenheit-Celsius in reverse order");
	for (fahr2 = 300; fahr2 >= 0; fahr2 = fahr2-20){
		printf("%4d %6.1f\n", fahr2, (5.0/9.0)*(fahr2-32));
	}
}
