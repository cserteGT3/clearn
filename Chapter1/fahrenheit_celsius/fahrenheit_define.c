#define	LOWER	0 // lower bound
#define	UPPER	300 // upper bound
#define	STEP	20 // step size

main()
{
	int fahr;
	for (fahr = LOWER; fahr <= UPPER; fahr = fahr + STEP)
		printf("%d\t%6.1f\n", fahr, (5.0/9.0)*(fahr-32));

}
