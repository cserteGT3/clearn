#include <stdio.h>
#include <math.h>

#define IN		1		/* inside a word */
#define OUT		0		/* outside a word */
#define MAXL	65		/* maximum word lemgth */
#define NMAX	90.0	/* number of #s in the histogram */

/* a program that creates a histogram from its input */
main()
{
	// max word length: MAXL
	int wordl[MAXL];
	// % of occurences
	float nhist[MAXL];
	// iterator, iterator, cuurent char, state, current word's length, number of words, maximum length
	int i, j, c, state, wl, nw, maxl;
	// the most common length
	int mostl;

	for (i = 0; i < MAXL; ++i) {
		wordl[i] = 0;
		nhist[i] = 0.0;
	}

	state = OUT;
	wl = nw = maxl = 0;

	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\t' || c == '\n') {
			state = OUT;

			if (wl < MAXL)
				++wordl[wl];
			else
				++wordl[MAXL-1];

			wl = 0;
		}
		else {
			if (state == OUT) {
				state = IN;
				++nw;
			}
			++wl;
		}
	}

	// search the longest word
	i = MAXL-1;
	while (i >= 0) {
		if (wordl[i] != 0) {
			maxl = i;
			break;
		}
		--i;
	}
	printf("word count: %d, longest word: %d\n", nw, maxl);

	// number of bins == length of the longest word
	mostl = 0;
	for (i = 0; i <= maxl; ++i) {
		if (mostl < wordl[i])
			mostl = wordl[i];
	}

	// % of every word length
	for (i = 0; i <=maxl; ++i) {
		nhist[i] = NMAX * wordl[i] / nw;
	}

	for (i = 0; i <= maxl; ++i) {
		for (j = 0; j < round(nhist[i]); ++j)
			printf("#");
		printf("\n");
	}
}
