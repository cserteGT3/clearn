#include <stdio.h>

#define IN	1	/* inside a word */
#define OUT	0	/* outside a word */

/* count lines, words, and characters in input
	actually, can't count lines, because...

main(){
	int c, nl, nw, nc, state;

	state = OUT;
	nl = nw = nc = 0;
	nl = 1; // after one line, the program terminates...
	while ((c = getchar()) != '\n') {
		++nc;
		if (c == '\n')
			++nl;
		if (c == ' ' || c == '\t' || c == '\n')
			state = OUT;
		else if (state == OUT) {
			state = IN;
			++nw;
		}

	}
	printf("%d %d %d\n", nc, nw, nl);


}
*/

/* a program that writes its input one word per line */

main(){
	int c, state;

	while ((c = getchar()) != '\n') {
		if (c == ' ' || c == '\t')
			printf("\n");
		else
			putchar(c);
	}
	printf("\n");
}
