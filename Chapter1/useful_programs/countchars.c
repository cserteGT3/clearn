#include <stdio.h>

/* count characters; 1st version */
main(){

	/* 1st version
	long nc = 0;

	while (getchar() != '\n'){
		nc++;
	}
	printf("%ld\n", nc);
	*/

	/* 2nd version
	double ncd;
	for (ncd = 0.0; getchar() != '\n'; ++ncd)
		;

	printf("%.0f\n", ncd);
	*/

	/* a program to count blanks/tabs in a line
	int nl, c;

	while ((c = getchar()) != '\n')
		if (c == '\t')
			++nl;

	printf("%d\n", nl);
	*/

	/* a program to copy its input to output,
		replacing each string of one or more blanks by a single blank*/

	int d = 0; //indicate if a space was preceeded by a space
	int c; //current char
	while ((c = getchar()) != '\n'){
		if ((c != ' ') || (d == 0)){
			putchar(c);
		}
		if (c == ' '){
			// its' a space
			d = 1;
		}
		else{
			// it's not a space
			d = 0;
		}
	}
	printf("\n");

}
