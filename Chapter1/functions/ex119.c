#include <stdio.h>
#define	MAXLINE	80	// maximum input line size

int getline2(char line[], int maxline);
void printrest();
void reverse(char s[]);

main()
{
	int len;					// current line length
	char line[MAXLINE];		// current input line

	while ((len = getline2(line, MAXLINE)) > 0) {
		reverse(line);
		printf("%s", line);
	}
	return 0;
}

/* getline2: read a line into s, return length
	getline() conflicts with stdio.h */
int getline2(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim-1 && (c = getchar()) !='\n' && c != EOF; ++i)
		s[i] = c;
	if (c == '\n') {
		s[i] = '\n';
		i++;
	}
	s[i] = '\0';
	return i;
}

// breaks if `s` doesn't contain '\0'
void reverse(char s[]) {
	int i, iend, c; // iterator, position of '\0'

	for (iend = 0; s[iend] != '\0'; ++iend)
		;

	for (i = 0; i < iend; ++i) {
		// we only want to change the string before '\0'
		--iend;
		if (i == iend)
			break;
		c = s[i];
		s[i] = s[iend];
		s[iend] = c;
	}
}
