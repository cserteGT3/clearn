#include <stdio.h>
#define	MAXLINE	100	// maximum input line size

int getline2(char line[], int maxline);
void copy(char to[], char from[]);

/* prnt longest input line */
main()
{
	int len;					// current line length
	int max;					// maximum length seen so far
	char line[MAXLINE];			// current input line
	char longest[MAXLINE];		// longest line saved here

	max = 0;
	while ((len = getline2(line, MAXLINE)) > 0)
		if (len > max) {
			max = len;
			copy(longest, line);
		}
	if (max > 0) // there was at least one line
		printf("longest: %d chars:\n%s\n", max, longest);

	return 0;
}

/* getline2: read a line into s, return length
	getline() conflicts with stdio.h */
int getline2(char s[], int lim)
{
	int c, i, n;

	for (i = 0; (c = getchar()) != EOF && c!='\n'; ++i)
		if (i <= lim-1) {
			s[i] = c;
			if (c == '\n') {
				s[i] = '\n';
				++i;
				s[i] = '\0';
				return i;
			}
		}
	n = i < lim-1 ? i : lim;
	s[n] = '\0';
	return i;
}

/* copy: 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
	int i;

	i = 0;
	while((to[i] = from[i]) != '\0')
		++i;
}
