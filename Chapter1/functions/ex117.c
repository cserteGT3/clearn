#include <stdio.h>
#define	MINLINE	80	// maximum input line size

int getline2(char line[], int maxline);
void printrest();

/* print every line longer than MINLINE */
/* structure
	while (no more lines)
		readlineuntil_MIN
		if length > MINLINE
				print read line and continue
*/

main()
{
	int len;					// current line length
	char line[MINLINE+1];		// current input line
	line[MINLINE] = '\0';

	while ((len = getline2(line, MINLINE)) != -1)
		if (len > MINLINE-1) {
			printf("%s", line);
			printrest();
		}
	return 0;
}

/* getline2: read a line into s, return length
	getline() conflicts with stdio.h */
int getline2(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim && (c = getchar()) !='\n'; ++i) {
		if (c == EOF)
			return -1;
		s[i] = c;
	}
	return i;
}

/* print until the end of the line */
void printrest()
{
	int c;
	while ((c = getchar()) != '\n' && c != EOF)
		putchar(c);
	printf("\n");
}
