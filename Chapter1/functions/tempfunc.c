#include <stdio.h>

#define	LOWER	0	// lower bound
#define	UPPER	100	//upper bound
#define	STEP	10	// step size

/* convert celsius to fahrenheit */
float c2f(float centigrade);

main()
{
	int t;
	for (t = LOWER; t <= UPPER; t+=STEP)
		printf("%d -> %6.1f\n", t, c2f(t));
	return 0;
}

/* compute celsius from fahrenheit */
float c2f(float centigrade)
{
	return centigrade/(5.0/9.0)+32.0;
}
